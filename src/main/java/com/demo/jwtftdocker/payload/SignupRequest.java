package com.demo.jwtftdocker.payload;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * Created by linhtn on 7/16/2021.
 */
@Getter
@Setter
public class SignupRequest {

    private String name;
    private String username;
    private String email;
    private String password;
    private Set<String> roles;

}
