package com.demo.jwtftdocker.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by linhtn on 7/16/2021.
 */
@Getter
@Setter
@AllArgsConstructor
public class MessageResponse {
    private String message;
}
